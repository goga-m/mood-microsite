FROM node:11.2.0
ENV NPM_CONFIG_LOGLEVEL warn
COPY . .
RUN npm run build
RUN npm install -g serve
CMD serve -s dist
EXPOSE 5000