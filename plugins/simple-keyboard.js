import Vue from 'vue'
import Keyboard from 'simple-keyboard'
import 'simple-keyboard/build/css/index.css'

Vue.use({
  install(Vue, options) {
    Vue.prototype.$keyboard = Keyboard
  }
})
